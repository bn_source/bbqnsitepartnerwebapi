﻿using BBQNPartnershipWebAPI.Helpers;
using BBQNPartnershipWebAPI.Model;
using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace BBQNPartnershipWebAPI.Services
{
    public interface ISitePartnerService
    {
        Task<PartnerSiteResponse> partnerSite(PartnerSiteRequest partnerSite);
    }


    public class SitePartnerService : ISitePartnerService
    {
        public static XmlDocument OptionDocISI = new XmlDocument();
        public static XmlDocument OptionDocOSI = new XmlDocument();
        public static XmlDocument OptionDocBI = new XmlDocument();

        private readonly AppSettings _appSettings;
        private readonly ILogger _logger;

        public SitePartnerService(IOptions<AppSettings> appSettings, ILogger logger)
        {
            _appSettings = appSettings.Value;
            _logger = logger.ForContext(this.GetType());
        }

        public Task<PartnerSiteResponse> partnerSite(PartnerSiteRequest partnerSite)
        {

            Database objDB;
            DbCommand objcmd;
            PartnerSiteResponse m = new PartnerSiteResponse();
            _logger.Information("PartnerSite Request object{@partnerSite}", partnerSite);

            #region declaration InsideSideImage
            OptionDocISI = new XmlDocument();
            XmlElement OptionElemISI;
            XmlNode OptionRootNodeISI = OptionDocISI.CreateElement("InsideSideImage");
            OptionDocISI.AppendChild(OptionRootNodeISI);
            #endregion

            #region declaration OutsideSideImage
            OptionDocOSI = new XmlDocument();
            XmlElement OptionElemOSI;
            XmlNode OptionRootNodeOSI = OptionDocOSI.CreateElement("OutsideSideImage");
            OptionDocOSI.AppendChild(OptionRootNodeOSI);
            #endregion

            #region declaration BuildingImage
            OptionDocBI = new XmlDocument();
            XmlElement OptionElemBI;
            XmlNode OptionRootNodeBI = OptionDocBI.CreateElement("BuildingImage");
            OptionDocBI.AppendChild(OptionRootNodeBI);
            #endregion

            try
            {
                objDB = new SqlDatabase(_appSettings.DbConnection);
                using (DbConnection con = objDB.CreateConnection())
                {
                    objcmd = objDB.GetStoredProcCommand("Spr_InsertSiteDetails");
                    objDB.AddInParameter(objcmd, "@Source", DbType.String, partnerSite.source);
                    objDB.AddInParameter(objcmd, "@ContactPersonName", DbType.String, partnerSite.contact_person_name);
                    objDB.AddInParameter(objcmd, "@ContactPhoneNumber", DbType.String, partnerSite.contact_mobile_number);
                    objDB.AddInParameter(objcmd, "@ContactEmail", DbType.String, partnerSite.contact_email_id);
                    objDB.AddInParameter(objcmd, "@ContactPersonType", DbType.String, partnerSite.contact_person_type);
                    objDB.AddInParameter(objcmd, "@BuildingType", DbType.String, partnerSite.building_type);
                    objDB.AddInParameter(objcmd, "@PropertyType", DbType.String, partnerSite.property_type);
                    objDB.AddInParameter(objcmd, "@Address", DbType.String, partnerSite.address);
                    objDB.AddInParameter(objcmd, "@SuperBuiltupAreaSqft", DbType.String, partnerSite.super_builtup_area_sq_ft);
                    objDB.AddInParameter(objcmd, "@CarpetAreaSqft", DbType.String, partnerSite.carpet_area_sq_ft);
                    objDB.AddInParameter(objcmd, "@FrontageSqft", DbType.String, partnerSite.frontage_sq_ft);
                    objDB.AddInParameter(objcmd, "@RoadWidthFront", DbType.String, partnerSite.road_width_in_front_meter);
                    objDB.AddInParameter(objcmd, "@SideRoadsWidth", DbType.String, partnerSite.width_of_side_roads_meter);
                    objDB.AddInParameter(objcmd, "@InvestmentCapability", DbType.String, partnerSite.investment_capability_in_fitout_interior_crores);

                    if(partnerSite.inside_site_image != null)
                    {
                        for(int i = 0; i < partnerSite.inside_site_image.Count; i++)
                        {
                            OptionElemISI = OptionDocISI.CreateElement("InsideImage");
                            OptionElemISI.SetAttribute("InsideTheSiteImage", Convert.ToString(partnerSite.inside_site_image[i]));

                            OptionRootNodeISI.AppendChild(OptionElemISI);
                        }
                    }

                    objDB.AddInParameter(objcmd, "@InsideTheSiteImage", DbType.Xml, OptionDocISI.InnerXml);

                    if (partnerSite.outside_site_image != null)
                    {
                        for (int i = 0; i < partnerSite.outside_site_image.Count; i++)
                        {
                            OptionElemOSI = OptionDocOSI.CreateElement("OutsideImage");
                            OptionElemOSI.SetAttribute("OutsideTheSiteImage", Convert.ToString(partnerSite.outside_site_image[i]));

                            OptionRootNodeOSI.AppendChild(OptionElemOSI);
                        }
                    }

                    objDB.AddInParameter(objcmd, "@OutsideTheSiteImage", DbType.Xml, OptionDocOSI.InnerXml);

                    if (partnerSite.building_image != null)
                    {
                        for (int i = 0; i < partnerSite.building_image.Count; i++)
                        {
                            OptionElemBI = OptionDocBI.CreateElement("BuildImage");
                            OptionElemBI.SetAttribute("BuildingImage", Convert.ToString(partnerSite.building_image[i]));

                            OptionRootNodeBI.AppendChild(OptionElemBI);
                        }
                    }


                    objDB.AddInParameter(objcmd, "@BuildingImage", DbType.Xml, OptionDocBI.InnerXml);

                    objDB.AddInParameter(objcmd, "@SiteVideo", DbType.String, partnerSite.site_video);
                    objDB.AddInParameter(objcmd, "@Comments", DbType.String, partnerSite.comments);
                    objDB.AddOutParameter(objcmd, "@out_msg", DbType.String, 100);
                    objDB.AddOutParameter(objcmd, "@out_msg_type", DbType.Boolean, 3);

                    objDB.ExecuteDataSet(objcmd);

                    m.message = Convert.ToString(objcmd.Parameters["@out_msg"].Value);
                    m.success = Convert.ToBoolean(objcmd.Parameters["@out_msg_type"].Value);
                }
            }
            catch (Exception ex)
            {
                _logger.Write(Serilog.Events.LogEventLevel.Error, ex.Message, "partnerSite Error");
                m.success = false;
                m.message = ex.Message;
            }
            finally
            {
                objDB = null;
                objcmd = null;
            }
            if (m.success == false)
                _logger.Debug("partnerSite Error {@m}", m);
            return Task.FromResult(m);
        }
    }
}
