﻿using BBQNPartnershipWebAPI.Helpers;
using BBQNPartnershipWebAPI.Model;
using Microsoft.Extensions.Options;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace BBQNPartnershipWebAPI.Services
{
    public interface IRevenueDataShareService
    {
        Task<RevenueDataSharingResponse> FunRevenueDataShare(RevenueDataSharingRequest revenueData);
    }
    public class RevenueDataShareService : IRevenueDataShareService
    {
        private readonly AppSettings _appSettings;
        private readonly ILogger _logger;

        public RevenueDataShareService(IOptions<AppSettings> appSettings, ILogger logger)
        {
            _appSettings = appSettings.Value;
            _logger = logger.ForContext(this.GetType());
        }

        public Task<RevenueDataSharingResponse> FunRevenueDataShare(RevenueDataSharingRequest revenueData)
        {
            Database objDB;
            DbCommand objcmd;
            DataTable ObjPriDt;
            RevenueDataSharingResponse m = new RevenueDataSharingResponse();
            List<RevenueDataSharing> revenueDataShare = new List<RevenueDataSharing>();
            _logger.Information("FunRevenueDataShare Request object{@revenueData}", revenueData);

            try
            {
                objDB = new SqlDatabase(_appSettings.UBQConnection);
                using (DbConnection con = objDB.CreateConnection())
                {
                    objcmd = objDB.GetStoredProcCommand("spr_FetchDeliverySalesForRevenueSharing");
                    objDB.AddInParameter(objcmd, "UserId", DbType.Int32, revenueData.user_id);
                    objDB.AddInParameter(objcmd, "FromDate", DbType.String, revenueData.from_date);
                    objDB.AddInParameter(objcmd, "ToDate", DbType.String, revenueData.to_date);
                    objDB.AddOutParameter(objcmd, "success", DbType.Boolean, 3);
                    objDB.AddOutParameter(objcmd, "msg", DbType.String, 100);
                    ObjPriDt =objDB.ExecuteDataSet(objcmd).Tables[0];
                }
                if (ObjPriDt.Rows.Count > 0)
                {
                    RevenueDataSharing revenue;
                    for(int i =0; i <ObjPriDt.Rows.Count; i++)
                    {
                        revenue = new RevenueDataSharing();
                        revenue.order_id = Convert.ToString(ObjPriDt.Rows[i]["OrderId"]);
                        revenue.merchant_ref_id = Convert.ToString(ObjPriDt.Rows[i]["merchant_ref_id"]); 
                        revenue.channel = Convert.ToString(ObjPriDt.Rows[i]["channel"]);
                        revenue.date = Convert.ToString(ObjPriDt.Rows[i]["date"]);
                        revenue.store_no = Convert.ToString(ObjPriDt.Rows[i]["StoreNo"]);
                        revenue.brand = Convert.ToString(ObjPriDt.Rows[i]["Brand"]);
                        revenue.store_name = Convert.ToString(ObjPriDt.Rows[i]["OutletDesc"]);
                        revenue.order_type = Convert.ToString(ObjPriDt.Rows[i]["order_type"]);
                        revenue.total_net_amount = Convert.ToDouble(ObjPriDt.Rows[i]["TotalNetAmount"]);
                        revenue.gross_sales = Convert.ToDouble(ObjPriDt.Rows[i]["GrossSales"]);
                        revenueDataShare.Add(revenue);

                    }
                    m.data = revenueDataShare;
                    m.success = Convert.ToBoolean(objcmd.Parameters["@success"].Value);
                    m.message = Convert.ToString(objcmd.Parameters["@msg"].Value);
                }
                else
                {
                    m.success = Convert.ToBoolean(objcmd.Parameters["@success"].Value);
                    m.message = Convert.ToString(objcmd.Parameters["@msg"].Value);
                }
            }
            catch (Exception ex)
            {
                _logger.Write(Serilog.Events.LogEventLevel.Error, ex.Message, "revenueData Error");
                m.success = false;
                m.message = ex.Message;
            }
            finally
            {
                objDB = null;
                objcmd = null;
                ObjPriDt = null;
            }
            if (m.success == false)
                _logger.Debug("revenueData Error {@m}", m);
            return Task.FromResult(m);
        }
    }
}
