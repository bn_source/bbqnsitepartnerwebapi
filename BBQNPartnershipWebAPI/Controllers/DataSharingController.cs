﻿using BBQNPartnershipWebAPI.Helpers;
using BBQNPartnershipWebAPI.Model;
using BBQNPartnershipWebAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBQNPartnershipWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [APIAuthorize]
    public class DataSharingController : ControllerBase
    {
        private IRevenueDataShareService _revenueDataShareService;

        public DataSharingController(IRevenueDataShareService revenueDataShareService)
        {
            _revenueDataShareService = revenueDataShareService;
        }

        /// <summary>
        /// It is used for send daily sales data to partners
        /// </summary>
        /// <param name="request">Procedure =Spr_InsertSiteDetails | FunRevenueDataShare</param>
        /// <returns></returns>
        [HttpPost]
        [Route("delivery-sales")]
        public async Task<IActionResult> FunRevenueDataShare(RevenueDataSharingRequest request)
        {
            var response = await _revenueDataShareService.FunRevenueDataShare(request);
            return Ok(response);
        }
    }
}
