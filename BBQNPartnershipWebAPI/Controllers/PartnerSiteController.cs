﻿using BBQNPartnershipWebAPI.Helpers;
using BBQNPartnershipWebAPI.Model;
using BBQNPartnershipWebAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBQNPartnershipWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [APIAuthorize]
    public class PartnerSiteController : ControllerBase
    {
        private ISitePartnerService _sitePartnerService;

        public PartnerSiteController(ISitePartnerService sitePartnerService)
        {
            _sitePartnerService = sitePartnerService;
        }

        /// <summary>
        /// It is used for save details site partner
        /// </summary>
        /// <param name="request">partnerSite | Procedure=Spr_InsertSiteDetails</param>
        /// <returns></returns>
       
        [HttpPost]
        [Route("update-detail")]
        public async Task<IActionResult> partnerSite(PartnerSiteRequest request)
        {
            var response = await _sitePartnerService.partnerSite(request);
            return Ok(response);
        }
    }
}
