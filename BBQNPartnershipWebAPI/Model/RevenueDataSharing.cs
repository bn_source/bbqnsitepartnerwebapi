﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBQNPartnershipWebAPI.Model
{
    public class RevenueDataSharingRequest
    {
        public int user_id{ get; set; }
        public string from_date { get; set; }
        public string to_date { get; set; }
    }
    public class RevenueDataSharingResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<RevenueDataSharing> data{ get; set; }
    }
    public class RevenueDataSharing
    {
        public string order_id { get; set; }
        public string merchant_ref_id { get; set; }
        public string channel { get; set; }
        public string date { get; set; }
        public string store_no { get; set; }
        public string brand { get; set; }
        public string store_name { get; set; }
        public string order_type { get; set; }
        public double total_net_amount { get; set; }
        public double gross_sales { get; set; }

    }
}
