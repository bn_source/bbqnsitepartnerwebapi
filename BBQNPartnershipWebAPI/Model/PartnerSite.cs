﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBQNPartnershipWebAPI.Model
{
    public class PartnerSiteRequest
    {
        //public string site_code { get; set; }
        public string source { get; set; }
        public string contact_person_name{ get; set; }
        public string contact_mobile_number{ get; set; }
        public string contact_email_id { get; set; }
        public string contact_person_type { get; set; }
        public string building_type { get; set; }
        public string property_type { get; set; }
        public string address { get; set; }
        public double super_builtup_area_sq_ft{ get; set; }
        public double carpet_area_sq_ft{ get; set; }
        public double frontage_sq_ft{ get; set; }
        public double road_width_in_front_meter{ get; set; }
        public double width_of_side_roads_meter{ get; set; }
        public double investment_capability_in_fitout_interior_crores{ get; set; }

        public List<string> inside_site_image { get; set; }

        public List<string> outside_site_image { get; set; }
        public List<string> building_image { get; set; }

        public string site_video { get; set; }
        public string comments { get; set; }
    }

    //public class InsideSiteImage
    //{
    //    public string inside_image { get; set; }
    //}

    //public class OutsideSiteImage
    //{
    //    public string outside_image { get; set; }
    //}

    //public class BuildingImage
    //{
    //    public string building_image { get; set; }
    //}
    public class PartnerSiteResponse
    {
        public string message { get; set; }
        public bool success { get; set; }
    }
}
