﻿using IdentityModel;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace BBQNPartnershipWebAPI.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class APIAuthorize : AuthorizeAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var rd = context.ActionDescriptor as ControllerActionDescriptor;
            int UserId = 8;
            string UserName = "admin";

            //Check the user claims and validate the access rights for the requested action method.
            string currentAction = rd.ActionName;
            string currentController = rd.ControllerName;
            var userClaims = context.HttpContext.User.Claims;
            UserId = Convert.ToInt32(userClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.Id).Value);
            UserName = userClaims.FirstOrDefault(x => x.Type == JwtClaimTypes.PreferredUserName).Value;

            //context.Result = new JsonResult("Forbidden") { StatusCode = (int)HttpStatusCode.Forbidden, Value = "You are not authorized to execute this action." };
            return;
        }
    }
}
