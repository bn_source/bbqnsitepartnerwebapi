﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BBQNPartnershipWebAPI.Helpers
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string DbConnection { get; set; }
        public string UBQConnection { get; set; }
    }
}
